/**
 * První vytvořená Java třída vůbec. Jako jediná není v žádném balíčku (zvaném
 * někdy <i>default package</i>).
 * 
 * @author libor
 *
 */
public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello a nice day!");
	}

}
