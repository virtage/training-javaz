package com.virtage.edu.javaz.k14gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class SwtMinimum {

	public static void main(String[] args) {

		Display d = new Display();
		Shell shell = new Shell(d);

		shell.setLayout(new FillLayout());

		Label label = new Label(shell, SWT.NONE);
		label.setText("Hello from SWT!");

		shell.setSize(100, 100);
		shell.open();

		// SWT smyčka
		while (!shell.isDisposed()) {
			if (!d.readAndDispatch()) {
				d.sleep();
			}
		}

		d.dispose();
	}

}
