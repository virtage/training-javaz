package com.virtage.edu.javaz.k14gui;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class SwingMinimum extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public SwingMinimum() {
		JLabel jlbHelloWorld = new JLabel("Hello from Swing!");
		add(jlbHelloWorld);
		this.setSize(100, 100);
		setVisible(true);
	}
	
	public static void main(String args[]) {
		new SwingMinimum();
	}
}