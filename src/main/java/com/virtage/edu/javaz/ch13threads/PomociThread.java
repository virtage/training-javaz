package com.virtage.edu.javaz.ch13threads;
public class PomociThread {

	public static void main(String[] args) {
		System.out.println("sysout");
		
		Vlakno v1 = new Vlakno("Prokop");
		Vlakno v2 = new Vlakno("Hubert");
		v1.start();
		v2.start();
	}

	/* třída definující vlákno */
	private static class Vlakno extends Thread {

		Vlakno(String jmeno) {
			super(jmeno);
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				System.out.println("Vlakno: " + getName());
				yield();
			}
		}
	}

}
