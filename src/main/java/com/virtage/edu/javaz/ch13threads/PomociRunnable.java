package com.virtage.edu.javaz.ch13threads;
import java.util.Random;


public class PomociRunnable {
	
	public static void main(String[] args) {
		Thread v1 = new Thread(new Vlakno("Karel"));
		Thread v2 = new Thread(new Vlakno("Ilja"));
		v1.start();
		v2.start();
	}
	
	/* třída definující vlákno */
	private static class Vlakno implements Runnable {
		private String jmeno;
		
		public Vlakno(String jmeno) {
			this.jmeno = jmeno;
		}

		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				System.out.println("Vlakno: " + jmeno);
				//Thread.yield();
				try {
					Thread.sleep(new Random().nextInt(10000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
