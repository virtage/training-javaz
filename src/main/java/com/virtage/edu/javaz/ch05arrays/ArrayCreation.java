package com.virtage.edu.javaz.ch05arrays;

@SuppressWarnings({ "unused" })
public class ArrayCreation {


	public static void main(String[] args) {
		// ** Array creation **

		// I. Declaration syntax:
		// 		a) int[] array -- recommended!
		// 		b) int array[]
		// 		c) int[] array[]

		// II. Initialization syntax:
		// 		a) new int[numOfElements]
		// 		b) { 10, 30, 40 } -- array initializator

		int[] array = { 30, 40, 60 };


		// Seperated array declaration and initialization
		int intArray[];		// = null;

		//System.out.println(intArray[3]);

		// ...
		intArray = new int[10];

		// Combined array declaration and initialization
		byte poleBajtu[] = new byte[10];
		poleBajtu[0] = (byte) 0;
		poleBajtu[1] = (byte) 1;
		poleBajtu[30] = (byte) 1;

	}
}
