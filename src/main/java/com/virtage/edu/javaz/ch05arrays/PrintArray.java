package com.virtage.edu.javaz.ch05arrays;

public class PrintArray {

	public static void main(String[] args) {

		// Check
		if (args.length == 0) {
			System.err.println("Supply some arguments!");
			return;
		}
		
		System.out.println("Number of supplied arguments: " + args.length);
		
		// Print arguments in < JDK 1.5
		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i]);
		}
		
		System.out.println("----------");
		
		// For-each (JDK 1.5+)
		for (String arg : args) {
			System.out.println(arg);
		}
	}

}
