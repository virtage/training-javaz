package com.virtage.edu.javaz.ch05arrays;

public class MultidimensionalArray {

	public static void main(String[] args) {
		// Int array of int arrays
		int inty[][] = new int[10][5];
		inty[3][0] = 30;
		inty[3][1] = 31;
		inty[3][2] = 32;

		// String arrays of string arrays
		String strings[][] = {
				new String[] { "1", "2",  "3",  "4" },
				new String[] { "I", "II", "III" }
		};
		
		
		System.out.println(inty[3][2]);
		System.out.println(strings[0][0]);		// 1
		System.out.println(strings[1][0]);		// I
		System.out.println(strings[1][1]);		// II
		
		//System.out.println(strings[1][4].length);	// error
		
	}
	
	
	
}
