package com.virtage.edu.javaz.ch05arrays;

import java.util.Arrays;

import com.virtage.edu.javaz.ch04methods.Rekurze;

public class AnonymousArray {

	public static void main(String[] args) {
		// Anonymous string array as argument
		String[] reverse = reverse(new String[]{ "Carl", "Tom", "Mike", "Richie" });
		System.out.println(Arrays.toString(reverse));
		
		// Idential to:
		String names[] = new String[2];
		names[0] = "joe";
		names[1] = "carl";
		reverse(names);
	}
	
	private static String[] reverse(String[] boys) {
		String result[] = new String[boys.length];
		
		for (int i = 0; i < boys.length; i++) {
			result[i] = Rekurze.reverse(boys[i]);
		}
		
		return result;
	}

}
