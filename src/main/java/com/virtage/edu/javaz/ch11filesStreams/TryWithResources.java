package com.virtage.edu.javaz.ch11filesStreams;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Příklad je přesání {@link ReadingFileJava6} s pomocí konstrukce JDK7
 * "try-with-resources".
 * 
 */
public class TryWithResources {
	
	private static final String FILE =  System.getProperty("user.home") +
			"/javaz/HelloWorld.java";					// change to your very first Java example

	public static void main(String[] args) {
		readingFile();
	}
	
	private static void readingFile() {
		// JDK7+ try-with-resources
		try (BufferedReader br = new BufferedReader(new FileReader(FILE))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
