package com.virtage.edu.javaz.ch11filesStreams;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NewDirectoryNIO2 {
	
	private static final String FOLDER_NAME = System.getProperty("user.home");

	public static void main(String[] args) {
		vytvoreni();
	}
	
	private static void vytvoreni() {
			try {
				Path path = Paths.get(FOLDER_NAME);
				Files.createDirectories(path);
				
			} catch (IOException e) {
				e.printStackTrace();
				
			}
	
	}

}
