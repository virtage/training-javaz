package com.virtage.edu.javaz.ch11filesStreams;
import java.io.File;

public class NewDirectoryOIO {
	
	private static final String FOLDER_NAME = System.getProperty("user.home");

	public static void main(String[] args) {
		create();
	}
	
	private static void create() {
		// Creates just a new instance, not a real file!
		File dir = new File(FOLDER_NAME);

		boolean isDirectoryCreated = dir.mkdir();

		if (isDirectoryCreated)
			System.out.println("Directory created successfully");
		else
			System.out.println("Directory was not created successfully but we don't know why");
	}
}
