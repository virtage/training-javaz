package com.virtage.edu.javaz.ch11filesStreams;


public class StreamClassesMethods {

	public static void main(String[] args) {
		
		// ** Byte-oriented streams **
		// InputStream:
		// int read()
		// int read(byte[] array)
		// int read(byte[] array, int index, int count)
		
		// OutputStream:
		// void write(int byte)
		// void write(byte[] array)
		// void write(byte[], int index, int count)
		
		
		// ** Character-oriented streams **
		// Reader:
		// int read();
		// int read(char[] array)
		// int read(char[] array, int index, int count)
		
		// Writer:
		// void write(int i)
		// void write(char[] array)
		// void write(char[], int index, int count)
		
	}

}
