package com.virtage.edu.javaz.ch11filesStreams;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadingFileJava6 {
	
	private static final String FILE =  System.getProperty("user.home") +
			"/git/training-javaz/src/main/java/HelloWorld.java";			// change to your very first Java example

	public static void main(String[] args) {
		readingFile();
	}

	private static void readingFile() {
		BufferedReader br = null;
		
		try {
			br = new BufferedReader(new FileReader(FILE));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} finally {
			try {
				if (br != null) br.close();
				
			} catch (IOException e) {
				e.printStackTrace();
				
			}
			
		}
		
	}
}
