package com.virtage.edu.javaz.ch04methods;

public class MetodyZaklady {

	// ** Deklarace metody **
	// [práva] [modifikátory] návratovýTyp jmenoMetody([parametry...]) [throws seznamVyjímek] {
	//     tělo metody
	// }

	// návratový typ: String, int ap. nebo void pro žádnou návratovou hodnotu
	// práva: public, protected, package-private, private
	// modifikátory: static, abstract, final, native, synchronized

	// ** Libovolné pořadí deklarace a volání (použití) metody **
	public static void main(String[] args) {
		System.out.println(rekniAhoj());
	}

	private static String rekniAhoj() {
		return "Ahoooj!";
	}

	// ** Návratový typ **
	private static int prictiJedna(int i) {
		return ++i;
	}

	private static int prictiDva(int i, int y) {
		return i + y;
	}
}
