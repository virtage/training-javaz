package com.virtage.edu.javaz.ch04methods;

public class VarargsSum {

	public static void main(String[] args) {
		System.out.println(sum());
		System.out.println(sum(10));
		System.out.println(sum(Math.random(), Math.random(), 30));

	}
	
	public static double sum(double ... numbers) {
		double result = 0;
		for (double number : numbers) {
			result += number;		// getProduct = getProduct + number;
		}
		return result;
	}
}
