package com.virtage.edu.javaz.ch04methods;

// Riziko rekurze - způsobí java.lang.StackOverflowError
public class PreteceniVolani {

	public static void main(String[] args) {
		metoda();
	}

	private static void metoda() {
		metoda();
	}
}
