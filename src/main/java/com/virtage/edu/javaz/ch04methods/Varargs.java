package com.virtage.edu.javaz.ch04methods;

import java.util.Arrays;

public class Varargs {

	public static void main(String ... args) {	// v bajtkódu bude String[] args
		// Postaru
		varargsPostaru(new String[] { "jedna" });			// 1 arg
		varargsPostaru(new String[] { "jedna", "dva" });	// 2 arg
		varargsPostaru(new String[] {});					// 0 arg

		// Ponovu
		varargsPonovu("jedna");				// 1 arg
		varargsPonovu("jedna", "dva");		// 2 arg
		varargsPonovu();					// 0 arg

		// Použití jako nepovinné argumenty
		povinneNepovinne(10);
		povinneNepovinne(10, 60, 90, 60);
	}


	// ** Varargs v < JDK 1.5 **
	private static void varargsPostaru(String[] args) {
		System.out.println("varargsPostaru(): " + Arrays.toString(args));
	}

	// ** Vararg v JDK 1.5+ **
	private static void varargsPonovu(String ... args) {
		System.out.println("varargsPonovu(): " + Arrays.toString(args));
	}


	private static void povinneNepovinne(int vek, int ... miry) {
	}
}
