package com.virtage.edu.javaz.ch04methods;

public class FormalniVsSkutecnyParametr {
	
	/**
	 * @param f je formální parametr
	 */
	private static int prictiJedna(int f) {
		return f + 1;
	}
	
	public static void main(String[] args) {
		// a je skutečný parametr (argument)
		int a = 10;
		System.out.println(prictiJedna(a));
	}
	
}
