package com.virtage.edu.javaz.ch04methods;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

public class PredavaniHodnotou {
	
	public static void main(String[] args) {
		// ** Předávání - primitivní typy **
		int i = 10;
		System.out.println("main(): i = " + i);		// stále 10
		zmenCislo(i);
		System.out.println("main(): i = " + i);		// stále 10
		
		System.out.println("-----------------------");
		
		// ** Předavani - referenční typy **
		// a) mutable (měnitelná) třída
		Calendar letos = new GregorianCalendar();
		System.out.println("main(): letos = " + letos.get(Calendar.YEAR));
		zmenRok(letos);
		System.out.println("main(): letos = " + letos.get(Calendar.YEAR));
		
		System.out.println("-----------------------");
		
		// b) immutable (neměnitelná) třída
		String mesto = "Uhlířské Janovice";
		System.out.println("main() mesto = " + mesto);
		zmenMesto(mesto);
		System.out.println("main() mesto = " + mesto);
	}
	
	private static void zmenCislo(int y) {
		int nahoda = new Random().nextInt();
		y = nahoda;
		System.out.println("zmenCislo(): Argument změněn na " + nahoda);
	}
	
	private static void zmenRok(Calendar datum) {
		datum.set(Calendar.YEAR, 1918);
		System.out.println("zmenDatum(): Argument zmene na " +
			datum.get(Calendar.YEAR));
	}
	
	private static void zmenMesto(String mesto) {
		mesto = "Horní Liptákov";
		System.out.println("zmenMesto(): Argment zmenen na " + mesto);
	}
	
	
}
