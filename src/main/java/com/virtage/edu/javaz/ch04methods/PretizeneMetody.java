package com.virtage.edu.javaz.ch04methods;

public class PretizeneMetody {

	public static void main(String[] args) {
		// ** přetížené (overloaded) metody **

		System.out.println(true);			// boolean
		System.out.println(3.14f);			// float
		System.out.println(3.14);			// double
		System.out.println("ahoj");			// String
		System.out.println("a");			// String o velikosti jednoho znaku
		System.out.println('a');			// char / znak

		System.out.println(formatujJmeno(true));
		System.out.println(formatujJmeno("Libor"));
		System.out.println(formatujJmeno("Libor", "Jelínek"));
		System.out.println(formatujJmeno("Libor", "Jelínek", 29));
	}

	private static String formatujJmeno(boolean muz) {
		return (muz == true) ? "Adam" : "Eva";
	}

	// Pozor: návratový typ ani modifikátory metody nejsou součástí
	// signatury metody, tj. nemohou být použity k rozlišení přetížených metod!
//	private static int formatujJmeno(boolean muz) {
//		return (muz == true) ? "Adam" : "Eva";
//	}

	private static String formatujJmeno(String jmeno) {
		return jmeno;
	}

	private static String formatujJmeno(String jmeno, String prijmeni) {
		return jmeno + " " + prijmeni;
	}

	private static String formatujJmeno(String jmeno, String prijmeni, int vek) {
		return jmeno + " " + prijmeni + " (věk " + vek + ")";
	}


}
