package com.virtage.edu.javaz.ch04methods;

public class Rekurze {
	
	private static int counter;
	private static final boolean LADIT = false;
	
	public static void main(String[] args) {
		if (LADIT) {
			System.out.println("VOLÁNÍ Č.\tBEZ 1. ZNAKU\tPRVNÍ ZNAK");
		}
		
		System.out.println("Výsledek je " + reverse("java"));
	}

	public static String reverse(String str) {
		// jeden znak nebo prázdný řetězec
        if (str.length() < 2) {
            return str;
        }
        
        String bezPrvnihoZnaku = str.substring(1);
        char prvniZnak = str.charAt(0);
        
        if (LADIT) {
        	System.out.format("%d. volání\t%s\t\t%s%n",
        		++counter, bezPrvnihoZnaku, prvniZnak);
        }
        
        return reverse(bezPrvnihoZnaku) + prvniZnak;

    }

}
