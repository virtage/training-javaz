package com.virtage.edu.javaz.ch04methods;

public class FinalniParametry {

	public static void main(String[] args) {

		System.out.println(poprej("Libor", 28));

	}

	private static String poprej(final String komu, final int kolik) {
		//komu = "Petře"; 			// chyba - nelze
		//kolik = 10;

		return "Blahopřání pro " + komu + " k " + kolik + ". narozeninám!";
	}

}