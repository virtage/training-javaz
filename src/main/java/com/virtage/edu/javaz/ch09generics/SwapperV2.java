package com.virtage.edu.javaz.ch09generics;

import com.virtage.edu.javaz.ch06objects.interfaces.Dog;
import com.virtage.edu.javaz.ch06objects.interfaces.Cat;
import com.virtage.edu.javaz.ch06objects.interfaces.Pet;

public class SwapperV2 {
	
	/*
	 * Takto napsaná metoda obsahuje
	 * (1) varování "The type parametr T1 is hiding the type T1"
	 * (2) kompilační chybu, protože nelze vytvářet instanci typového parametru
	 */
	// public <T1 extends Pet, T2 extends Pet> T2 vymen(T1 savec1) { // (1)
	//		T2 savec2 = new T2(); 										 // (2)
	//		savec2.setName(savec1.getName());
	//		return savec2;
	// }
	
	// [modif] <paramTyp> paramTyp name([paramTyp|neparamTy]...)
	
	// private int vynasobDvema(int)
	// private float vynasobDvema(float)
	// private short vynasobDvema(short)
	// --->
	// private <T> T vynasobDvema(T) {
	//			return T * 2;
	//	}
	
	
	// [modifikatory] <T> navratovy_typ jmeno_metody([parametry])
	// public static <T> void vypis(T typ) {
	// 			sysout(typ);
	// }
	
	public static <T extends Pet> T swap(T mammal1, T mammal2) {
		mammal2.setName(mammal1.getName());
		return mammal2;
	}
	
	public static void main(String[] args) {
		// Vyměníme kočku za psa
		Cat micka = new Cat("Micka");
		Dog alik = new Dog("Alik");
		
		Pet dogocat = swap(micka, alik);
		//Pet kockopes2 = vymen(new GregorianCalendar(), alik);
		
		// vypíše "Dog jménem Micka"
		System.out.println("Dog jménem " + dogocat.getName());
	}
}
