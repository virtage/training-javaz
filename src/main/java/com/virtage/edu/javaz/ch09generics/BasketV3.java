package com.virtage.edu.javaz.ch09generics;

/**
 * Typově bezpečná verze košíku.
 *
 * @param <T> Jakýkoli typ, který má košík nést.
 */
public class BasketV3<T> {

	private T value;

	public void setValue(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public static void main(String[] args) {
		// Primitive nelze použít jako typový parametr!
		// KosikV3<int> k1 = new KosikV3<>();
		BasketV3<Integer> k1 = new BasketV3<>();
		//k1.setValue("dsds");			// chyba typové kontroly
		//k1.setValue(10.5);			// chyba typové kontroly
		k1.setValue(10);
		@SuppressWarnings("unused")
		int intVal = k1.getValue();
		// byte byteVal = k1.getValue();	// chyba takové kontroly
		
		
		BasketV3<Integer> basketForIntegers;
		BasketV3<String> basketForStrings;
		//...
		
	}

}
