package com.virtage.edu.javaz.ch09generics;

import com.virtage.edu.javaz.ch06objects.interfaces.Cat;
import com.virtage.edu.javaz.ch06objects.interfaces.Dog;

public class SwapperV1 {
	
	public Cat swap(Dog dog) {
		Cat cat = new Cat("Micka");
		cat.setName(dog.getName());
		return cat;
	}
	
	public Dog swap(Cat cat) {
		Dog dog = new Dog("Alík");
		dog.setName(cat.getName());
		return dog;
	}
}
