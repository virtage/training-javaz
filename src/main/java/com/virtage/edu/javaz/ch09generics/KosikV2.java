package com.virtage.edu.javaz.ch09generics;

/**
 * Řešení s použitím Object.
 * 
 *
 */
public class KosikV2 {
	
	private Object value;
	
	public Object getValue() {
		return value;
	}
	public void setValue(Object newValue) {
		this.value = newValue;
	}
	
	
	public static void main(String[] args) {
		KosikV2 k1 = new KosikV2();
		k1.setValue("String");
		k1.getValue();
		
		// Neošetřené přetypování
		KosikV2 k2 = new KosikV2();
		//k2.setValue("Tonda");
		k2.setValue(10);
		
		String velkymi = (String) k2.getValue();
		System.out.println(velkymi.toUpperCase());
		
	}

}
