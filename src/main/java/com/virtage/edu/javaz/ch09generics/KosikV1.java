package com.virtage.edu.javaz.ch09generics;

/**
 * Řešení mnoha přetížených metod.
 * 
 *
 */
public class KosikV1 {
	
	private byte byteValue;
	private short shortValue;
	private int intValue;
	private long longValue;
	private float floatValue;
	private double doubleValue;
	private Object objectValue;
	
	public byte getByteValue() {
		return byteValue;
	}
	
	public void setByteValue(byte byteValue) {
		this.byteValue = byteValue;
	}
	public short getShortValue() {
		return shortValue;
	}
	public void setShortValue(short shortValue) {
		this.shortValue = shortValue;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public long getLongValue() {
		return longValue;
	}
	public void setLongValue(long longValue) {
		this.longValue = longValue;
	}
	public float getFloatValue() {
		return floatValue;
	}
	public void setFloatValue(float floatValue) {
		this.floatValue = floatValue;
	}
	public double getDoubleValue() {
		return doubleValue;
	}
	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}
	public Object getObjectValue() {
		return objectValue;
	}
	public void setObjectValue(Object objectValue) {
		this.objectValue = objectValue;
	}
	
	
	public static void main(String[] args) {
		KosikV1 k1 = new KosikV1();
		k1.setIntValue(15);
		k1.getIntValue();
		
		KosikV1 k2 = new KosikV1();
		k2.setObjectValue("String");
		k2.getObjectValue();
		
		//...
	}
}
