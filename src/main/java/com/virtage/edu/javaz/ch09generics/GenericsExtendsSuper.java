package com.virtage.edu.javaz.ch09generics;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class GenericsExtendsSuper {

	public static void main(String[] args) {
		// ? -------------------------------------------------------
		List<LocalDate> l0_1 = Arrays.asList(LocalDate.of(1918, 12, 24),
				LocalDate.now());
		vypisKolekci(l0_1);

		List<String> l0_2 = Arrays.asList("Tonda", "Eva");
		vypisKolekci(l0_2);

		// čtení (? extends) ---------------------------------------
		List<Number> l1_1 = Arrays.asList(10, 20.5);
		double soucet = sectiKolekci(l1_1);
		System.out.println(soucet);

		List<Float> l1_2 = Arrays.asList(10f, 20.5f);
		soucet = sectiKolekci(l1_2);
		System.out.println(soucet);

		// zápis (? super) ------------------------------------------
		List<Number> l2_1 = new ArrayList<>();
		pridejDoKolekce(l2_1);
		System.out.println(l2_1);

		List<Object> l2_2 = new ArrayList<>();
		pridejDoKolekce(l2_2);
		System.out.println(l2_2);
	}

	private static void vypisKolekci(Collection<?> c) {
		System.out.println("-- vypisKolekci(): -------------------------");

		for (Object o : c)
			System.out.println(o);
	}


	private static double sectiKolekci(Collection<? extends Number> c) {
		System.out.println("-- sectiKolekci(): -------------------------");
		
		double soucet = 0;
		for (Number prvek : c)
			soucet = soucet + prvek.doubleValue();
		return soucet;
	}

	private static void pridejDoKolekce(List<? super Number> c) {
		System.out.println("-- pridejDoKolekce(): -----------------------");
		
		c.add(3.14);
		c.add(3.14f);
		c.add(3);
		c.add((short) 3);
		c.add(new BigDecimal(30));
		
	}
}
