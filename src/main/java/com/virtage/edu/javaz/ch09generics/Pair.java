package com.virtage.edu.javaz.ch09generics;

public class Pair<K, V> {
	
	private K key;
	private V value;

	public K getKlic() {
		return key;
	}
	
	public void setKlic(K klic) {
		this.key = klic;
	}

	public V getHodnota() {
		return value;
	}
	
	public void setHodnota(V hodnota) {
		this.value = hodnota;
	}
	

	public static void main(String[] args) {
		Pair<Integer, String> arabskyRimsky = new Pair<>();
		arabskyRimsky.setKlic(52);
		arabskyRimsky.setHodnota("LII");
		
		Pair<String, String> komici = new Pair<>();
		komici.setKlic("Laurel");
		komici.setHodnota("Hardy");
	}

}
