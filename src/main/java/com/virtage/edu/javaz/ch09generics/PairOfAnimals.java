package com.virtage.edu.javaz.ch09generics;

import com.virtage.edu.javaz.ch06objects.interfaces.Animal;
import com.virtage.edu.javaz.ch06objects.interfaces.Cat;
import com.virtage.edu.javaz.ch06objects.interfaces.Dog;
import com.virtage.edu.javaz.ch06objects.interfaces.Drawable;

public class PairOfAnimals<K extends Animal & Drawable, V extends Animal> {

	private K klic;
	private V hodnota;

	public K getKlic() {
		return klic;
	}

	public void setKlic(K klic) {
		this.klic = klic;
	}

	public V getHodnota() {
		return hodnota;
	}

	public void setHodnota(V hodnota) {
		this.hodnota = hodnota;
	}


	public static void main(String[] args) {
		PairOfAnimals<Dog, Cat> d1 = new PairOfAnimals<>();
		d1.setKlic(new Dog("Alík"));
		d1.setHodnota(new Cat("Micka"));


		PairOfAnimals<Dog, Cat> d2 = new PairOfAnimals<>();
		d2.setKlic(new Dog("Alík"));
		d2.setHodnota(new Cat("Micka"));


		// Pet neimplementuje Drawable, jak vyžaduje zápis
		// první parametrický typ (K extends Animal & Drawable)
//		DvojiceZvirat<Pet, Pet> d3 = new DvojiceZvirat<>();
//		d3.setKlic(new Dog("Tomáš"));
//		d3.setHodnota(new Cat("Jana"));
	}

}
