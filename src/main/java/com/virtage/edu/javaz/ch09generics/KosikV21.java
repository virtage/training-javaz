package com.virtage.edu.javaz.ch09generics;

import java.util.Date;

/**
 * Řešení s použitím Object.
 * 
 *
 */
public class KosikV21 {
	
	private Object value;
	
	public Object getValue() {
		return value;
	}
	public void setValue(Object newValue) {
		if ((this.value != null) && (!this.value.getClass().equals(newValue.getClass())))
			System.err.println("Type mismatch! You are trying to insert "
					+ newValue.getClass() + " to the basket of type " +
					this.value.getClass() + "!");
		
		this.value = newValue;
	}
	
	
	public static void main(String[] args) {
		KosikV21 k1 = new KosikV21();
		k1.setValue("String");
		k1.setValue("another String");
		
		// Neošetřené přetypování
		KosikV21 k2 = new KosikV21();
		k2.setValue("String");
		k2.setValue(new Date());				// error!
	}

}
