package com.virtage.edu.javaz.ch09generics;

import java.util.ArrayList;
import java.util.List;

public class Erasure<K> {

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.add("Hi");
		String x = list.get(0);

		// is compiled into
		List list2 = new ArrayList();
		list2.add("Hi");
		String x2 = (String) list.get(0);

		// thus there's no way how to find out that it ways List of Strings!

	}
}