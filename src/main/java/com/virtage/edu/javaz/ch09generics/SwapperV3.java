package com.virtage.edu.javaz.ch09generics;

import com.virtage.edu.javaz.ch06objects.interfaces.Cat;
import com.virtage.edu.javaz.ch06objects.interfaces.Dog;

/** Vyměňuje P za Q. */
public class SwapperV3<P, Q> {

	public Q swap(P something) {
		// Chyba "Cannot instantiate the type Q"
		//Q q = new Q();					// Object q = new ((Dog) Object())

		// Chyba "The method getName() is undefined for the type P"
		//q.setName(P.getName());

		return null;
	}

	public static void main(String[] args) {
		SwapperV3<Cat, Dog> vymenovac = new SwapperV3<>();

		vymenovac.swap(new Cat("micka"));

	}
}
