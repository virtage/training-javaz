package com.virtage.edu.javaz.ch09generics;

public class BasketV0 {
	private int value;
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
}
