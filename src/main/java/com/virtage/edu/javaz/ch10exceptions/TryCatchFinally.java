package com.virtage.edu.javaz.ch10exceptions;

public class TryCatchFinally {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Vyzkoušejte různé hodnoty jména a věku
		String name = "Carl";				// "", null, "Thomas", etc.
		int age = -28;						// -30, 151, etc.

		System.out.format("Is there an person with name '%s' in age of %d?%n", name, age);

		boolean exists = false;
		
		
		try {
			exists = PersonRegistry.isThereSuchPerson(name, age);

		// From the most specific to the most generic
		//} catch (Throwable t) {}

		} catch (InvalidNameException e) {
			e.printStackTrace();

		} /* catch (InvalidAgeException e) {
			// Unchecked (runtime) exception
			e.printStackTrace();

		} */ catch (Exception e) {
			System.out.println(e.toString());

		} finally {
			System.out.println("Goodbye (finally block)!");

		}

	}

}
