package com.virtage.edu.javaz.ch10exceptions;

import java.net.MalformedURLException;
import java.net.URL;

public class Throws {
	
	public static void main(String[] args) throws MalformedURLException {
		System.out.println(getHomepage());
	}
	
	private static URL getHomepage() throws MalformedURLException {
		return new URL("http//www.virtage.cz");
	}

}
