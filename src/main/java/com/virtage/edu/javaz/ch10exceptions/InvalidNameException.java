package com.virtage.edu.javaz.ch10exceptions;

public class InvalidNameException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidNameException() {
		super("Invalid name (blank string or null)");
	}
	
	public InvalidNameException(String badName) {
		super("Bad name was " + badName + "! Name must contain first and last name.");
	}

}
