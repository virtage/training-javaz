package com.virtage.edu.javaz.ch10exceptions;

public class TryFinally {
	
	public static void main(String[] args) {
		
		try {
			Integer.parseInt("3,14");			// instead of 3.14
			
		} catch (NumberFormatException e) {
			System.out.println("Invalid number format!");
			
			new RuntimeException("Runtime eception from catch block");
			
		} finally {
			System.out.println("Finally is always executed!");
			
		}
		
		System.out.println("------------------");
		
		try {
			Integer.parseInt("3,14");		// throws exception but finally is executed anyway
			
		} finally {
			System.out.println("Finally is always executed!");
			
		}
		
		
	}

}
