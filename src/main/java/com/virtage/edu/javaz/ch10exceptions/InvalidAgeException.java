package com.virtage.edu.javaz.ch10exceptions;

public class InvalidAgeException extends IllegalArgumentException {
	
	private static final long serialVersionUID = 8936873476252229197L;
	
	public InvalidAgeException(int age) {
		// set detail message using parent's constructor
		super("Invalid age " + age);
	}
	
	public String getDetail() {
		return "....";
	}
}
