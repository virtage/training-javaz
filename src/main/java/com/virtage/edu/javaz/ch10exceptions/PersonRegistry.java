package com.virtage.edu.javaz.ch10exceptions;

public class PersonRegistry {

	public static boolean isThereSuchPerson(String name, int age)
			throws InvalidNameException, InvalidAgeException {
		
		InvalidNameException invalidNameException = new InvalidNameException();
		
		if ((name == null) || (name.isEmpty())) {
			throw invalidNameException;
		}

		// Name doesn't containing space (firstname <space> surname) is invalid
		if (!name.contains(" ")) {
			throw new InvalidNameException(name);
		}

		if ((age < 0) || (age > 150)) {
//			InvalidAgeException exception = new InvalidAgeException(vek);
//			throw exception;
			throw new InvalidAgeException(age);
		}

		return true;
	}
	
	
	public static void main(String[] args) throws InvalidNameException, InvalidAgeException {
		isThereSuchPerson("libor", 30);
		
		//isThereSuchPerson(args[0], Integer.parseInt(args[1]));
	}

}
