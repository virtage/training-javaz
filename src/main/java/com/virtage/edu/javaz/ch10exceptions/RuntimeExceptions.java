package com.virtage.edu.javaz.ch10exceptions;

public class RuntimeExceptions {

	public static void main(String[] args) {
		someMethod();
	}

	private static void someMethod() /*throws NumberFormatException*/ {
		Integer.parseInt("10");
		Integer.parseInt("hello");
	}
}
