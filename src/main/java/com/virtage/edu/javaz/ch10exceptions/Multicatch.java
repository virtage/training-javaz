package com.virtage.edu.javaz.ch10exceptions;

public class Multicatch {

	public static void main(String[] args) {
		// Vyzkoušejte různé hodnoty jména a věku
		String name = "Carl";				// "", null, "Thomas", etc.
		int age = 28;						// -30, 151, etc.
		
		System.out.format("Is there an person with name '%s' and age of %d?%n", name, age);
		
		try {
			System.out.println(PersonRegistry.isThereSuchPerson(name, age));
			
		} catch (InvalidNameException | InvalidAgeException e) {
			e.printStackTrace();
			
		} finally {
			System.out.println("Goodbye (finally block)!");
		
		}
	}

}
