package com.virtage.edu.javaz.ch02basics;

public class TerminalovyIO {

	public static void main(String[] args) {

		// System.out.println() a print()
		System.out.println("ahoj");			// "ahoj" + nový řádek
		System.out.print("ahoj");			// "ahoj" bez odřákování



		// ** Metoda format() **
		System.out.format("Muj vek je %d. Mé jméno je %s.%n", 29, "Libor");		// dekaticky
		System.out.format("Muj vek je %o. Mé jméno je %s.%n", 29, "Libor");		// oknalově
		System.out.format("Muj vek je %x. Mé jméno je %s.%n", 29, "Libor");		// hexa
		// Stejné jako:
		//System.out.println("Muj vek je " + 29 + ". Mé jméno je " + "Libor");

		System.out.format("Hodnota Ludolfova čísla je %f.%n", Math.PI);
		System.out.format("Hodnota Ludolfova čísla je %.2f.%n", Math.PI);

		// Tisk znaku procento
		System.out.format("Učíme se na 110%%.%n");


		// Znak nového řádku v Javě
		System.out.format("%n");
		System.out.println("\n");
		System.out.println();
		System.getProperty("line.separator");
		System.lineSeparator();

		System.out.println(System.getProperty("os.name"));


		//TODO ** Čtení s konzole **

	}

}
