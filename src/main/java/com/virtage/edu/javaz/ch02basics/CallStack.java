package com.virtage.edu.javaz.ch02basics;

public class CallStack {

	public static void main(String[] args) {
		method1();
	}

	private static void method1() {
		method2();
	}

	private static void method2() {
		method3();
	}

	private static void method3() {
		System.out.println("method3()");
	}
}
