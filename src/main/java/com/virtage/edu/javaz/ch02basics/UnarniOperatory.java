package com.virtage.edu.javaz.ch02basics;

public class UnarniOperatory {

	public static void main(String[] args) {
		int i = 0;		
		System.out.println(i++);		// 0
		System.out.println(i);			// 1
		
		System.out.println("-----------------");
		
		int y = 0;		
		System.out.println(++y);		// 1
		System.out.println(y);			// 1

	}

}
