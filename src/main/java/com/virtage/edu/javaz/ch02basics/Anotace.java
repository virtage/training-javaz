package com.virtage.edu.javaz.ch02basics;

@SuppressWarnings("unused")					// Anotace třídy
public class Anotace {
	
	@Deprecated								// Anotace členské proměnné
	public static String name = "Johny";
	
	public static String firstName = "Johny";
	
	
	@Override								// Anotace metody
	public String toString() {
		return "I'm example of Java annotations";
	}
	
	// Anotace parametru metody
	private static void sayHello(int vek, @Deprecated String name) {
		System.out.println("Hello world!");
	}
	
	public static void main(String[] args) {
	//	sayHello(33, name);
	}

}
