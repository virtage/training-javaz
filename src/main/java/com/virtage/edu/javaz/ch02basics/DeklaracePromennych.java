package com.virtage.edu.javaz.ch02basics;

public class DeklaracePromennych {
	
	// Syntaxe:
	// [modifikátory] typ identif [ = inic ] [, identif [= inic]
	// Modifikátory:
	// a) oprávnění:  private, public, protected
	// b) final (= konstanta)
	// c) static
	
	final static double PI = 3.14;
	final static double PRUMER = 30.1;
	
	static int staticka = 10;			// třídní prom.
	int instancni = 20;						// instanční prom.
	
	public static void main(String[] args) {
		int i = 10;
		
		
		// Statická prom.
		System.out.println(staticka);
		System.out.println(DeklaracePromennych.staticka);
		
		// Instanční prom.
		System.out.println(new DeklaracePromennych().instancni);
		
	}

}
