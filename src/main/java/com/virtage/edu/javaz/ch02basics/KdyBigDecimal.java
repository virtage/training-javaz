package com.virtage.edu.javaz.ch02basics;

import java.math.BigDecimal;

public class KdyBigDecimal {

	public static void main(String[] args) {
		// Jak chybně počítá float nebo double
		// Mělo by vypsat 0.61, ale vypíše 0.6100000000000001
		System.out.println(1.03 - 0.42);

		// Pro přesné výpočty (peníze ap.) vždy použijte BigDecimal!
		BigDecimal bd = new BigDecimal(1.03);
		System.out.println(bd.subtract(new BigDecimal(0.42)));
		
		
	}
}
