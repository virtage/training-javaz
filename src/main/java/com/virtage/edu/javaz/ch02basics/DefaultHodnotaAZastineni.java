package com.virtage.edu.javaz.ch02basics;

public class DefaultHodnotaAZastineni {
	
	// Fields (member variables)
	private static byte b /* = 0 */;
	private static short s;
	private static int i;
	private static long l;
	private static float f;
	private static double d;
	private static boolean bool /* = false */;
	private static String S /* = null */;
	
	public static void main(String[] args) {
		System.out.println("== Třídní proměnné ==");
		System.out.println("byte = " + b);
		System.out.println("short = " + s);
		System.out.println("int = " + i);
		System.out.println("long = " + l);
		System.out.println("float = " + f);
		System.out.println("double = " + d);
		System.out.println("boolean = " + bool);
		System.out.println("String = " + S);
		
		// Lokální proměnné zastiňují (hides) členské proměnné
		byte b = -30;
		short s = 23;
		int i = 100;
		long l = 100L;
		float f = 3.14f;
		double d = 3.14d;
		boolean bool = true;
		String S = null;
		
		System.out.println("== Lokální proměnné ==");
		System.out.println("byte = " + b);
		System.out.println("short = " + s);
		System.out.println("int = " + i);
		System.out.println("long = " + l);
		System.out.println("float = " + f);
		System.out.println("double = " + d);
		System.out.println("boolean = " + bool);
		System.out.println("String = " + S);
		
		System.out.println("== Znovu třídní proměnné ==");
		System.out.println("byte = " + DefaultHodnotaAZastineni.b);
		System.out.println("short = " + DefaultHodnotaAZastineni.s);
		System.out.println("int = " + DefaultHodnotaAZastineni.i);
		System.out.println("long = " + DefaultHodnotaAZastineni.l);
		System.out.println("float = " + DefaultHodnotaAZastineni.f);
		System.out.println("double = " + DefaultHodnotaAZastineni.d);
		System.out.println("bool = " + DefaultHodnotaAZastineni.bool);
		System.out.println("String = " + DefaultHodnotaAZastineni.S);
		
	}
	
	public void bloky2() {
		int i1 = 10;
		{
			i1 = 20;
			{
				i1 = 30;
			}
		}
		
	}

}
