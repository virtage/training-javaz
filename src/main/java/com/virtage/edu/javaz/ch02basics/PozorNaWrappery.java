package com.virtage.edu.javaz.ch02basics;

public class PozorNaWrappery {

	public static void main(String[] args) {
		porovnaniPrimitives();
		porovnaniWrappers();
	}

	private static void porovnaniPrimitives() {
		int i1 = 0;
		int i2 = 0;

		for (; i1 == i2; i1++, i2++);

		System.out.println("Přestalo se rovnat při " + i1);
	}

	private static void porovnaniWrappers() {
		Integer i1 = 0;
		Integer i2 = 0;

		for (; i1 == i2; i1++, i2++);

		System.out.println("Přestalo se rovnat při " + i1);
	}
}
