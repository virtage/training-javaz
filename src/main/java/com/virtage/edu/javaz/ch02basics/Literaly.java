package com.virtage.edu.javaz.ch02basics;


@Deprecated
public class Literaly {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Celočíselné literály - int (defaultně), long
		int i1 = 1002002;
		int i2 = 0x38abc;			// hexa
		int i3 = 3_102_009;			// oddělování řádů v JDK7+
		int i4 = 067;				// oktalově
		int i5 = 0b101;				// binárně

        long l1 = 3102l;			// stejné jako l1 = (long) 3102
		long l2 = 3102L;			// stejné jako l2 = (long) 3102
        long l3 = 100;

		// Znakové - jen char
		char c1 = 'š';
		char c11 = 'ऒ';				// znak "fň" :-)
		char c2 = '\u0912';			// unicode sekvence pro ऒ (fň)
		char c3 = '\r';      		// escape sekvence (neplést i unicode sekvencí)

		// Řetězové - vždy String
		String s1 = "AHoj jak \"se\" máte?";		// když potřebujem uvést ", použijem \"
		String s2 = "Jmeno\tPrijemni\n\tBydliště";	// vypíše "Jmeno     Prijmeni
													//                   Bydliště

		// Desetinná - float, double (defaulně)
		float f1 = 3.1444f;			// "Normálně" (semilogaritmický).
		float f2 = 3e5F; 	  		// Vědecny (expotenciální tvar) čteme 3*10^5.
       	//float f3 =  3.14;			// Chyba - nelze přiřadit double do float (větší do menšího).

		// Malé nebo velké D
		double d1 = 3.14d;
		double d4 = 3.14D;
		double d2 = 3e19D;			// je identické = 3*10^19
		double d3 = 3.14;			// Bez značky je to pořád double

		// Logický - boolean
		boolean b1 = true;
		boolean b2 = false;

		// Class
		Class<?> clazz = String.class;

		// Výpisy
		System.out.println("c2 = " + c2);
		System.out.println("c3 = " + c3);
		System.out.println("s2 = " + s2);
		System.out.println("clazz = " + clazz);
	}

}