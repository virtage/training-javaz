package com.virtage.edu.javaz.ch02basics;

public class PrimitiveWrappers {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// ** Autoboxing ** (od JDK 1.5+)

		// Obálka do primitive
		int i = new Integer(32);
		char c = new Character((char) 120);

		// Primitive do obálky
		Integer I = 32;
		Character C = 'z';

		System.out.println(i);
		System.out.println(c);
		System.out.println(I);
		System.out.println(C);


		// ** Klasicky ** (v < JDK 1.5)

		// převody
		int i2 = new Integer(10).intValue();
		float f1 = new Float(3.14).floatValue();
		float f2 = new Float("3.14").floatValue();

		// ** Metadata o primitivním typu **
		System.out.println("Float má velikost max. " + Float.MAX_VALUE + " ");


		/*
		PRIMITIVE:		OBÁLKA:
		int   			Integer
		char  			Character
		long 			Long
		byte 			Byte
		short 			Short
		float 			Float
		double 			Double
		*/
	}

}
