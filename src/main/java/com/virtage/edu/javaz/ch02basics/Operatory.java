package com.virtage.edu.javaz.ch02basics;

import java.util.Date;

public class Operatory {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// operator
		//   |
		// 1 + 2
		// |   |
		// operand

		// ** Operátory přiřazení (assignment) **
		// Primivní typy
		int x = 10;
		int y = 20;
		int z = x + y;

		// ** Přetypovací **
		// syntaxe: (cílový typ) zdroj
		// primitives
		long i1 = 10L;

		// pozor na priority!
		double d1 = (double) 3.14 + 0.5;
		// porovnej s
		double d2 = (double) (3.14 + 0.5);


		// ** Aritmetické **
		// binární +, -, *, /, %
		int i2 = 10 + 10;
		int i3 = 10 - 10;
		int i4 = 10 * 10;
		int i5 = 100 / 10;								// 10
		int i6 = 105 % 10;			// dělení modulo (zbytek po dělení)	// 5

		// unární +, -, od++, ++od, od--, --od
		int i7 = 10;	// 10
		i7++;			// 11
		i7--;			// 10
		++i7;			// 11
		--i7;			// 10

		int i8 = i7++;	// i8 = 10;    i7 = 11;
		int i9 = ++i7;  // i9 = 12;    i7 = 12;


	    // i++          i = i + 1
		// i--          i = i - 1

		// ** Relační **
		// >, >=, <, <=, ==, !=, instanceof
		// primitives
		boolean b1 = 10 > 5;	// větší, než / greater than
		boolean b2 = 10 >= 10;	// větší nebo rovno / greater or equal
		boolean b3 = 5 < 10;	// menší, než / less than
		boolean b4 = 5 <= 5;	// menší nebo rovno / less or equal
		boolean b5 = 10 == 11;	// rovno / equal
		boolean b6 = 10 != 5;	// nerovno / non-equal

		// referenční typy - instanceof
		boolean b7 = "ahoj" instanceof String;			// je tohoto typu?
		boolean b8 = "ahoj" instanceof CharSequence;	// implementuje rozhraní?
		String ahoj = "ahoj";
		boolean b9 = ahoj instanceof String;
		// Nelze porovnávat nesouvisející příbuzenské linie
//		int vek = 30;
//		boolean b10 = vek instanceof String;
		Number vek = new Integer(10);
		boolean b9_1 = vek instanceof Float;
//		Date dnes = new Date();
//		boolean b9_2 = dnes instanceof Float;


		// ** Logické operátory ** && (AND, logický součin),
		//                         || (OR, log. součet),
		//                         ! (NOT)
		// primitives
		boolean b10 = true && true; 		// true
		boolean b11 = true && false;		// false
		boolean b12 = false && false;		// false (zkracené vyhodnocování)
		boolean b13 = true || false;		// true
		boolean b14 = false || false;		// false
		boolean b15 = !false;				// true
		boolean b16 = !true;				// false


		// ** Bitové operátory **  >>, <<, >>>, &, |, ^, ~
		// vynecháváme...

		// ** Podmíněný  (conditional) (terciální) operátor **  od ? od : od
		// boolean jeDeset = (i == 10) ? true : false;
		// if (i == 10)
		//    jeDeset = true;
		// else
		//    jeDeset = false;
		int mujVek = 0;
		int vekNaFormular = (mujVek == 0) ? -1 : mujVek;

		// refereční typ
		String mojeJmeno = null;
		String jmenoNaFormular = (mojeJmeno == null) ? "<nevyplněno>" : mojeJmeno;

		/*
		if ((mojeJmeno == null) && (mojeJmeno == ""))			// HRUBÁ CHYBA!!
		if ((mojeJmeno == null) && (mojeJmeno.equals("")))
			return "<nevyplněno>";
		else
			return mojeJmeno;
		*/


		// ** new **
		// pro primitives neexistuje
		// jen referenční typy!
		int i10 = new Integer(5);


		// ** Zkrácené operátory **
		int xy = 10;
						// stejné jako:
		xy += 10;		// xy = xy + 10;
		xy -= 10;		// xy = xy - 10;
		xy *= 10;		// xy = xy * 10;
		xy /= 10;		// xy = xy / 10;
		xy %= 10;		// xy = xy % 10;
	}

}
