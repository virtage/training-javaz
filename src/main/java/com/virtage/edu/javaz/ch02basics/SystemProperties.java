package com.virtage.edu.javaz.ch02basics;

import java.util.Properties;

public class SystemProperties {

	public static void main(String[] args) {
		// Systémové proměnné NEJSOU totéž proměnné prostředí!!!

		// Zabudovaná, např. domovská složka uživatele:
		System.out.println(System.getProperty("user.home"));

		// ** Nastavení **

		// a) přímo v programu:
		System.setProperty("name", "karel");

		// b) z příkazové řádky:
		// $ java -Djmeno=Karel NaseHlavniTrida
		//    nikoli
		// $ java NaseHlavniTrida -Djmeno=Karel


		// ** Čtení **
		System.out.println("Dnes je " + System.getProperty("den") +
				" a svátek má " + System.getProperty("name"));


		// ** Výpis všech system properties **
		Properties properties = System.getProperties();
		properties.list(System.out);
	}

}
