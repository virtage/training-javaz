package com.virtage.edu.javaz.ch02basics;

/**
 * Javadoc komentář pro celou třídu. Můžete používat
 * <code>HTML</code> značky!
 *
 * @author libor
 * @see http://znalosti.virtage.cz/java/index
 */
public class Komentare {

	/** PI konstanta */
	public static final double PI = 3.14;

	/**
	 * Javadoc komentář hlavní spouštění speak
	 *
	 * @param args
	 */
	/*
	 * Běžný blokový komentář
	 */
	public static void main(String[] args) {
		// Jednořádkový komentář
		// druhý řádek
		// třetí...

		/*
		blokový
		komentář
		*/
	}

}
