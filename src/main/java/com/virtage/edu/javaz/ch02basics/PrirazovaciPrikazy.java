package com.virtage.edu.javaz.ch02basics;

public class PrirazovaciPrikazy {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		int a = 1, b = 2, c = 3;		// více proměnných na jednom řádku

		a = b + c * 2;  // přiřazovací příkaz
		b += 5;         // přiřazovací příkaz
		a++;            // inkrementace
		System.gc();    // volání metody
		
		{
			{
				{
					{
						
					}
				}
			}
			
		}
	}

}
