package com.virtage.edu.javaz.a01register;

import java.util.ArrayList;
import java.util.List;

public final class Register {
	
	private static List<Person> history = new ArrayList<>();
	
	/** Nobody can instantiate this class */
	private Register() {}
	
	public static boolean find(Person person) {
		if (person.getAge() < 0) {
			throw new IllegalArgumentException("Invalid person age");
		}
		
		history.add(person);
		
		return true;
	}
	
	public static List<Person> getHistory() {
		return history;
	}
}
