package com.virtage.edu.javaz.a01register;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Person joe = new Person("Joe", 30);
		Person sally = new Person("Sally", 10);

		List<Person> persons = Arrays.asList(joe, sally);
		/*
		List<Person> persons = new ArrayList<>();
		persons.add(joe);
		persons.add(sally);
		 */

		for (Person p : persons) {
			Register.find(p);
		}

		System.out.println(Register.getHistory());
	}
}
