package com.virtage.edu.javaz.ch07packages.permissions.b;

import com.virtage.edu.javaz.ch07packages.permissions.a.ClassA;

public class ClassB {
	
	public static void main(String[] args) {
		//ClassA.protectedI;
		
		class ClassA1 extends ClassA {
			private void method() {
				System.out.println("protected was = " + protectedI);
			}
		}
	}
}
