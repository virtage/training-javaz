package com.virtage.edu.javaz.ch07packages.permissions.a;

public class ClassA {
	private   static int privateI;
	          static int packageDefaultI;
	protected static int protectedI;
	public    static int publicI;
}
