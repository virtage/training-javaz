package com.virtage.edu.javaz.ch07packages;

import static java.util.Collections.min;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StaticImport {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>();
		
		list.add(1);
		list.add(2);
		list.add(3);
		
		// Note min() is missing declaring class
		System.out.println(min(list));
		
		// If we would omit static import...
		System.out.println(Collections.min(list));

	}

}
