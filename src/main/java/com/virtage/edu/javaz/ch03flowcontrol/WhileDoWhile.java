package com.virtage.edu.javaz.ch03flowcontrol;

public class WhileDoWhile {

	public static void main(String[] args) {
		// ** while -- cyklus s podmínkou na začátku **
		// syntaxe: while (výraz) příkaz
		int n = 0;
		while (n < 10) {
			System.out.println(n++);		// vypíše 0...9
		}

		// napsáno jako for
		//for (int n2 = 0; n2 < 10; n2++) {
		//	System.out.println(n2);
		//}

		System.out.println("-------");

		// ** do-while -- cyklus s podmínkou na konci **
		// syntaxe: do příkaz while (výraz);
		int m = 100;
		do {
			System.out.println(m++);		// vypíše 0...9
		} while (m < 10);

	}

}
