package com.virtage.edu.javaz.ch03flowcontrol;

public class Switch {

	public static void main(String[] args) {
		// Proč používat switch?
		// Tento zápis se dá přesat
//		if (10 > 5) {
//			System.out.println("ano");
//		} else if (10 > 8) {
//			System.out.println("ne");
//		} else if (10 > 9) {
//			;
//		} else {
//			System.out.println("nic");
//		}

		// Fall-trough vyhodnocování

		// Napsáno se switch v < JDK7
		int DEN = 2;
		switch (DEN) {
			case 1: {
				System.out.println("pondělí");
				break;
			}

			case 2: System.out.println("úterý");
					break;

			case 3: System.out.println("středa");
					break;

			default:	System.out.println("jiný den");
		}

		System.out.println("--------");

		// Napsáno se switch v JDK7+ (můžeme využívat stringy)
		String den = "Tuesday";
		switch (den) {
			case "Monday": {
					System.out.println("pondělí");
					break;
			}
			case "Tuesday": {
					System.out.println("úterý");
					break;
			}
			case "Wednesday": System.out.println("středa");
					break;
			case "Thursday":
				System.out.println("čtvrtek");
				break;
			case "Friday":
				System.out.println("pátek");
				break;

			// OR s řetězci používat nelze (bude chyba)
			//case "Saturday" || "Sunday":
			//	System.out.println("víkend");

			case "Saturday":
			case "Sunday":
				System.out.println("víkend");
				break;

			default:
				System.out.println("neznámý den");
		}
	}

}
