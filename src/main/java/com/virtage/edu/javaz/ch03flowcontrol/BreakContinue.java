package com.virtage.edu.javaz.ch03flowcontrol;

public class BreakContinue {

	public static void main(String[] args) {
		String[] tyden = { "pondělí", "úterý", "středa", "čtvrtek", "pátek" };
		
		// ** Break **
		// Hledání malého pátku v poli -- vypíše pondělí až čtvrtek
		for (String den : tyden) {
			System.out.println(den);
			if (den.equals("čtvrtek"))
				break;
		}
		
		System.out.println("-----------------------------");

		// ** Continue **
		String retezec = "petr pletl příliš svetr";
		int max = retezec.length();
		int pocetP = 0;

		for (int i = 0; i < max; i++) {
			// není-li 'p', přeskoč zbytek těla cyklu
			if (retezec.charAt(i) != 'p')
				continue;

			// zvěč počítadlo
			pocetP++;
		}
		System.out.println("Nalezeno " + pocetP + " výskytů 'p'.");
	}

}
