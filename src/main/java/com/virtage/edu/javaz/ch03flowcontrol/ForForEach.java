package com.virtage.edu.javaz.ch03flowcontrol;

import java.util.ArrayList;
import java.util.List;

public class ForForEach {

	public static void main(String[] args) {
		// ** Příkaz cyklu for ** -- alternativou k while
		// syntaxe: for ([inic]; výraz; [iter]) příkaz
		for (int n = 0; n < 10; n++) {
			System.out.println(n);				// vypíše 0...9
		}

		System.out.println("-----------------");

		// ** Příkaz "for-each" **
		// syntaxe: for (klic : kolekceNeboPole) příkaz
		// pro pole i kolekce stejná syntaxe

		// a) pole
		String[] tyden = { "pondělí", "úterý" };
		for (String den : tyden) {
			System.out.println(den);
		}

		System.out.println("-----------------");

		// b) kolekce
		List<String> seznam = new ArrayList<>();
		seznam.add("jedna");
		seznam.add("dva");

		for (String cislo : seznam) {
			System.out.println(cislo + " má pozici " + seznam.indexOf(cislo));
		}

	}
}
