package com.virtage.edu.javaz.ch03flowcontrol;

public class If {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Dead code -- větev "else" se nikdy neprovede!
		if (10 > 5) 
			System.out.println("ano");
			// System.out.println("ještě ano");
		else
			System.out.println("ne");

		// Druhá syntaxe if
		if (10 > 5) {
			System.out.println("ano");
		} else {
			System.out.println("ne");
		}

		// Třetí podoba
		if (10 > 5) {
			System.out.println("ano");
		} else if (10 > 8) {
			System.out.println("ne");
		} else {
			System.out.println("nic");
		}

		// Podmíněný výraz lze použít místo if-else
		String vysledek = (10 > 5) ? "ano" : "ne";
	}

}
