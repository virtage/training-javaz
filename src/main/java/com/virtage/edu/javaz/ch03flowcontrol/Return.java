package com.virtage.edu.javaz.ch03flowcontrol;

public class Return {

	// return - vyskoč z metody
	// return <hodnota> - vyskoč a vrať hodnotu z metody

	public static void main(String[] args) {
		// Vypíše jen 0-5
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
			if (i == 5) {
				return;		// break by zde měl stejný význam
			}
		}

		// Nebude nikdy zavoláno!
		System.out.println("Moje jméno je " + dejJmeno());
	}

	private static String dejJmeno() {
		return "Libor";
	}

}
