package com.virtage.edu.javaz.ch08stringsAndTime;

public class ToStringMethod {

	public static void main(String[] args) {
		
		System.out.println(new ToStringMethod());
		// is actually equivalent to
		System.out.println(new ToStringMethod().toString());
		// so "I'm ToStringMethod class" will be printed twice
	}
	
	
	
	@Override
	public String toString() {
		return "I'm ToStringMethod class";
	}

}
