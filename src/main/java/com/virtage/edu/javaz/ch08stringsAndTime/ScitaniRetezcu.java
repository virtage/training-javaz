package com.virtage.edu.javaz.ch08stringsAndTime;

import java.util.Date;

@SuppressWarnings("unused")
public class ScitaniRetezcu {

	public static void main(String[] args) {
		// *** Použití operátoru "+" pro referenční typ?! ***
		// Sčítání primitivních typů je ok
		int v = 1 + 1;
		
		// Sčítání polí?!
		int a1[] = new int[] { 1, 2, 3 };
		int a2[] = new int[] { 4, 5, 6 };
		//int a3 = a1 + a2;					// nelze
		
		// Sčítání objektů, např. Date
		Date d1 = new Date();
		Date d2 = new Date();
		//Date d3 = d1 + d2;					// nelze
		
		// Sčítání řetězců
		String s1 = "Popoca";
		String s2 = "tépetl";
		String mexicanVolcano = s1 + s2;		// jak to že jde, když String je objekt?!
		System.out.println(mexicanVolcano);
		
		// Alternativou k "+" je concat()
		System.out.println("dark".concat("ness"));
	}

}
