package com.virtage.edu.javaz.ch08stringsAndTime;

import java.util.Arrays;

public class RetezecNaBajty {

	public static void main(String[] args) {

		// ** Převod řetezce na pole bajtů ***

		// Zápis
		String den = "Tuesday";

		// je identický s
		//    new String("Tuesday");

		// nebo
		//    new String(new byte[] { 84, 117, 101, 115, 100, 97, 121 };

		// Pole bajtů získáme z každého řetezce metodou getBytes()
		byte[] bytes = den.getBytes();
		System.out.println(Arrays.toString(bytes));


		if ("ahoj" == "ahoj");
		// if (new String("ahoj").equals(new String("ahoj")))




		// StringBuffer (ve všech Javách)
		long bufferStart =System.currentTimeMillis();
		StringBuffer buffer = new StringBuffer("ahoj");
		buffer.append(" libor");
		buffer.append(" tomáši");
		buffer.append(" karle");
		long bufferEnd =System.currentTimeMillis();

		System.out.println("StringBuffer trval " + (bufferEnd - bufferStart));

		// "ahoj" + " libor" + " tomáš" + " karel";


		// StringBuilder
		long builderStart =System.currentTimeMillis();
		StringBuilder builder = new StringBuilder("ahoj");
		builder.append(" libor");
		builder.append(" tomáši");
		builder.append(" karle");
		long builderEnd =System.currentTimeMillis();

		System.out.println("StringBuilder trval " + (builderEnd - builderStart));


	}

}
