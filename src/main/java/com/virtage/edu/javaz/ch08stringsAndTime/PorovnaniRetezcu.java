package com.virtage.edu.javaz.ch08stringsAndTime;

public class PorovnaniRetezcu {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		if ("hello" == "hello") {
			System.out.println("TRUE: string == string");
		} else {
			System.out.println("FALSE: string == string");
		}

		if (new String("hello") == new String("hello")) {
			System.out.println("TRUE: new String() == new String()");
		} else {
			System.out.println("FALSE: new String() == new String()");
		}

		if (new String("ahoj").equals(new String("ahoj"))) {
			System.out.println("TRUE: new String().equals(new String())");
		} else {
			System.out.println("FALSE: new String().equals(new String())");
		}
	}

}
