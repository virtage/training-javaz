package com.virtage.edu.javaz.ch06objects.inheritance;

/**
 * Příklad nemožnosti dědit z finální třídy jako je např. {@link Math}.
 *
 * @author JELL
 *
 */
public class FinalClassFromSubclass /* extends Math */ {

}
