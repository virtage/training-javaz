package com.virtage.edu.javaz.ch06objects.interfaces;

public class Dog /* extends java.lang.Object */ implements Pet, Drawable {
	
	private String name;
	
	public Dog(String name) {
		this.name = name;
	}

	public void woof() {
		System.out.println("wooof!!!");
	}
	
	@Override
	public int numOfLegs() {
		return 4;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void draw() {
		// ASCII art from http://www.chris.com/ascii/index.php?art=animals/dogs
		System.out.println("                            __");
		System.out.println("     ,                    ,\" e`--o");
		System.out.println("    ((                   (  | __,'");
		System.out.println("     \\~----------------' \\_;/");
		System.out.println("hjw  (                      /");
		System.out.println("     /) ._______________.  )");
		System.out.println("    (( (               (( (");
		System.out.println("     ``-'               ``-'");

	}
}