package com.virtage.edu.javaz.ch06objects.inheritance;

/** Čtyrúhelník */
public abstract class Quadrangle extends Shape {
	
	protected double a;
	double b;
	double c;
	double d;

	public Quadrangle(double aa, double bb, double c, double d) {
		a = aa;
		b = bb;
		this.c = c;
		this.d = d;
	}
	
	@Override
	public double getCircumference() {
		return a + b + c + d;
	}
	
	// getArea() must be defined in children
	
}
