package com.virtage.edu.javaz.ch06objects.inheritance;

public class Square extends Quadrangle {

	public Square(double side) {
		super(side, side, side, side);
	}

	// getCircumference() from parent is okay

	@Override
	public double getArea() {
		// All sides are of same lenght
		return Math.pow(super.a, 2);
	}
	
	
	
	
	

	public static void main(String[] args) {
		System.out.println(new Square(10).getArea()); // 100.0
		System.out.println(new Square(10).getCircumference()); // 40.0
	}

}
