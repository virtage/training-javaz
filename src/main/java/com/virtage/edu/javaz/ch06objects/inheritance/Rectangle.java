package com.virtage.edu.javaz.ch06objects.inheritance;

/** Obdélník */
public class Rectangle extends Quadrangle {

	public Rectangle(double a, double b) {
		super(a, b, a, b);
	}
	
	// getCircumference();		/* is NOT equal to */		super.getCircumference();


	@Override
	public double getArea() {
		return super.a * super.b;
	}
	
//	@Override
//	public double getCircumference() { return 3.14;	}
	
	
	public static void main(String[] args) {
		Rectangle o = new Rectangle(10, 5);
		System.out.println(o.getArea());			// 100.0
		System.out.println(o.getCircumference());			// 30.0
	}

}
