package com.virtage.edu.javaz.ch06objects.interfaces;

public interface Animal {

	/*public static final*/ int NUM_OF_HEADS = 1;

	/*public abstract*/ int numOfLegs();
}