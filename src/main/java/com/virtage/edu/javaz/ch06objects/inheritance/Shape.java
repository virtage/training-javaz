package com.virtage.edu.javaz.ch06objects.inheritance;

/** Útvar */
public abstract class Shape {

	/** Obvod */
	public abstract double getCircumference();

	/** Obsah */
	public abstract double getArea();
	
	public final void info() {
		System.out.println("I'm " + this.getClass().getSimpleName());
	}
	
	public static void main(String[] args) {
		//new Shape();			// you cannot instantiate abstract class
	}
	
}
