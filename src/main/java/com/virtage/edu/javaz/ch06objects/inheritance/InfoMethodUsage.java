package com.virtage.edu.javaz.ch06objects.inheritance;

public class InfoMethodUsage {

	public static void main(String[] args) {
		Rectangle o = new Rectangle(10, 20);
		o.info();
		
		Square c = new Square(30);
		c.info();
	}
}
