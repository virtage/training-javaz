package com.virtage.edu.javaz.ch06objects;

public class AnonymousClasses {
	
	public static class Neanderthal {
		public void makeSound() {
			System.out.println("grrrr");
		}
	}
	
	public static class NeathSayingHello extends Neanderthal {
		@Override
		public void makeSound() {
			System.out.println("hellooo");
		}
	}
	
	public static void main(String[] args) {
		
		// Sample usage
		Neanderthal neanderthalSayingHello = new Neanderthal() {
			@Override
			public void makeSound() {
				System.out.println("helloooo");
			}
		};
		
		neanderthalSayingHello.makeSound();
		
		// or as method argument
		speak(new Neanderthal() {
			@Override
			public void makeSound() {
				System.out.println("hiiiii");
			}
		});
	}
	
	
	public static void speak(Neanderthal neadr) {
		neadr.makeSound();
	}
	
}
