package com.virtage.edu.javaz.ch06objects;

// POJO = Plain Old Java Object - class not implementing or extending at all
public class PersonJavaBean {

	private String name;
	private int age;
	private int numOfLegs = 2;

	// read-write property "name"
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// read-write property "age"
	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	// read-only propety "numOfLegs"
	public int getNumOfLegs() {
		return this.numOfLegs;
	}
}
