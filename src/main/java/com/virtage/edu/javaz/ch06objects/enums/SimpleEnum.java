package com.virtage.edu.javaz.ch06objects.enums;

public class SimpleEnum {
		
	public static enum Sex {
		MALE, FEMALE;
	}
	
	public static double averageAge(Sex sex) {
		if (sex == Sex.MALE) {
			return 70.5;
		} else {
			return 87.2;
		}
	}
	
	public static void main(String[] args) {
		System.out.println("Average male age is " + averageAge(Sex.MALE));
	}
}