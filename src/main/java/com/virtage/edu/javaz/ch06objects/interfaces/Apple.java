package com.virtage.edu.javaz.ch06objects.interfaces;

public class Apple implements Drawable {

	@Override
	public void draw() {
		System.out.println("        __");
		System.out.println("        \\/.--,");
		System.out.println("        //_.'");
		System.out.println("   .-\"\"-/\"\"-- ..");
		System.out.println("  /          __ \\");
		System.out.println(" /           \\\\\\ \\");
		System.out.println(" |            || |");
		System.out.println(" \\               /");
		System.out.println(" \\  \\            /");
		System.out.println("  \\  '-         /");
		System.out.println("   '-.__.__. ''  sjw");	
	}

}
