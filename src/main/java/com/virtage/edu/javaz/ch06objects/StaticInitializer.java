package com.virtage.edu.javaz.ch06objects;

public class StaticInitializer {

	public static void main(String[] args) {
		// static initializer will be called always before first usage of class field or method
		System.out.println("pi = " + MathConstants.PI);

		// c-or will be called only upon "new" operator
		new MathConstants();

		System.out.println("pi = " + MathConstants.PI);

		new MathConstants();
	}
}

class MathConstants {

	public static double PI;

	static {
		PI = 3.14;
		System.out.println("hello from static initializer");
	}

	public MathConstants() {
		System.out.println("hello from c-or (constructor)");
	}

}
