package com.virtage.edu.javaz.ch06objects.interfaces;

public class Cat implements Pet, Drawable {
	
	private String jmeno;
	
	public Cat(String jmeno) {
		this.jmeno = jmeno;
	}
	
	@Override
	public int numOfLegs() {
		return 4;
	}
	
	@Override
	public void setName(String name) {
		this.jmeno = name;
	}

	@Override
	public String getName() {
		return this.jmeno;
	}

	@Override
	public void draw() {
		// From http://www.chris.com/ascii/index.php?art=animals/cats
		System.out.println("            _,'|             _.-''``-...___..--';)");
		System.out.println("           /_ \'.      __..-' ,      ,--...--'''");
		System.out.println("          <\\    .`--'''       `     /'");
		System.out.println("           `-';'               ;   ; ;");
		System.out.println("     __...--''     ___...--_..'  .;.'");
		System.out.println("    (,__....----'''       (,..--''   Felix Lee <flee@cse.psu.edu>");
	}

}
