package com.virtage.edu.javaz.ch06objects.enums;

public enum WeekDays {
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}