package com.virtage.edu.javaz.ch06objects.interfaces;

import java.util.ArrayList;
import java.util.List;

public class Painter {

	// Example of poloymorphism
	public static void main(String[] args) {
		List<Drawable> list = new ArrayList<>();
		
		list.add(new Apple());
		list.add(new Dog("Alík"));
		list.add(new Cat("Micka"));
		list.add(Spider.newInstance());
		
		for (Drawable item : list) {
			item.draw();
		}
	}

}
