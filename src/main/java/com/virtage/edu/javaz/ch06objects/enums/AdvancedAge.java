package com.virtage.edu.javaz.ch06objects.enums;

public class AdvancedAge {

	private static enum Sex {
		MALE(70.5), FEMALE(87.2);

		private double averageAge;

		private Sex(double averageAge) {
			this.averageAge = averageAge;
		}

		public double averageAge() {
			return averageAge;
		}
	}

	public static void main(String[] args) {
		System.out.println("Male average age is " + Sex.MALE.averageAge());

		// Compare with "SimpleEnum":
		//System.out.println("Male average age is " + averageAge(Sex.MALE));
	}

}
