package com.virtage.edu.javaz.ch06objects.interfaces;

public class Spider implements Animal, Drawable {
	
	private Spider() {}
	
	public static Spider newInstance() {
		return new Spider();
	}
	
	@Override
	public int numOfLegs() {
		return 8;
	}
	
	@Override
	public void draw() {
		System.out.println("          ^^         |         ^^");
		System.out.println("          ::         |         ::");
		System.out.println("   ^^     ::         |         ::     ^^");
		System.out.println("   ::     ::         |         ::     ::");
		System.out.println("    ::     ::        |        ::     ::");
		System.out.println("      ::    ::       |       ::    ::");
		System.out.println("        ::    ::   _/~\\_   ::    ::");
		System.out.println("          ::   :::/     \\:::   ::");
		System.out.println("            :::::(       ):::::");
		System.out.println("                  \\ ___ /");
		System.out.println("             :::::/`   `\\:::::");
		System.out.println("           ::    ::\\o o/::    ::");
		System.out.println("         ::     ::  :\":  ::     ::");
		System.out.println("       ::      ::   ` `   ::      ::");
		System.out.println("      ::      ::           ::      ::");
		System.out.println("     ::      ::             ::      ::  R. Nykvist (Chuckles)");
		System.out.println("     ^^      ::             ::      ^^");
		System.out.println("             ::             ::");
		System.out.println("             ^^             ^^");
	}

}
