package com.virtage.edu.javaz.ch06objects.interfaces;

public interface Pet extends Animal {
	
	void setName(String name);
	String getName();
	
}
