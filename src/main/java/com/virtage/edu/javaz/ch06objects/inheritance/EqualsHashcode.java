package com.virtage.edu.javaz.ch06objects.inheritance;

class Mesto {
	private String nazev;

	public Mesto(String nazev) {
		this.nazev = nazev;
	}

	public String getNazev() {
		return nazev;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nazev == null) ? 0 : nazev.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesto other = (Mesto) obj;
		if (nazev == null) {
			if (other.nazev != null)
				return false;
		} else if (!nazev.equals(other.nazev))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Mesto [nazev=" + nazev + "]";
	}

}

public class EqualsHashcode {

	public static void main(String[] args) {

		Mesto praha = new Mesto("Praha");
		Mesto takyPraha = new Mesto("Praha");

		if (praha == takyPraha) {
			System.out.println("praha == takyPraha je true");
		} else {
			System.out.println("praha == takyPraha je false");
		}

		System.out.println("---------------");

		if (praha.equals(takyPraha)) {
			System.out.println("praha.equals(takyPraha) je true");
		} else {
			System.out.println("praha.equals(takyPraha) je false");
		}

	}

}
