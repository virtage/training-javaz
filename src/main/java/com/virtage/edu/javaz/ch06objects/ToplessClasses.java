package com.virtage.edu.javaz.ch06objects;

/**
 * Top-level class (also outer or enclosing class).
 */
public class ToplessClasses {

	/**
	 * Nested (vnořená) class is static member of outer class.
	 */
	public static class NestedClass {
	}

	/**
	 * Inner (vnitřní) class is instance member of outer class.
	 */
	public class InnerClass {
	}

	/**
	 * Anonymous class doesn't have a name.
	 */
	public InnerClass anonymousClass = new InnerClass() {
	};
	
	public void metoda() {
		/** Local class exists only inside method as any other local variables. */
		class LocalClass {
			int a, b;
		}
	}
}

/**
 * Class in file of other class. These classes cannot be public
 *
 * Třída v souboru jiné třídy. Tato třída však nesmí být veřejná (podle veřejné
 * třídy se musí jmenovat soubor).
 */
class ClassInOtherClassFile {}
