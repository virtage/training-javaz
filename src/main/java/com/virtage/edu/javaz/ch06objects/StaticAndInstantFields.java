package com.virtage.edu.javaz.ch06objects;

/** Násobič */
class Multiplier {

	/** Násobitel */
	public static int multiplier = 2;

	/** Násobenec */
	public int multiplicand;

	/** Součin */
	public int getProduct() {
		return multiplicand * multiplier;
		// or fully qualified syntax:
		//return this.multiplicand * Multiplier.multiplier;
	}
	
	public static int getMultiplier() {
		return multiplier;
	}

	@Override
	public String toString() {
		return "Multiplier { multiplicand=" + multiplicand + " }";
	}
}

public class StaticAndInstantFields {

	public static void main(String[] args) {

		Multiplier multiplierByTwo = new Multiplier();
		Multiplier.multiplier = 2;
		multiplierByTwo.multiplicand = 10;
		System.out.println(Multiplier.getMultiplier());			//
		System.out.println(multiplierByTwo.getProduct());		// 20


		Multiplier multiplierByTen = new Multiplier();
		Multiplier.multiplier = 10;
		multiplierByTen.multiplicand = 10;
		System.out.println(multiplierByTen.getProduct());		// 100

		// Static field "multiplier" is shared among all instances
		// This is why later change also affect already existing instances
		System.out.println(multiplierByTwo.getProduct());		// 100 (10 * 10)
	}

}