package com.virtage.edu.javaz.ch12collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CollectionAndArrayConversion {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		// Převod kolekce --> pole pomocí toArray() seznamu a množin
		Collection<String> c1 = new ArrayList<>();
		Object[] array = c1.toArray();
		String[] array2 = c1.toArray(new String[] {});


		// Převod pole --> kolekce pomocí Arrays.asList()
		String[] names = { "Tomáš", "Karel", "Tomáš", "Luděk" };
		List<String> l1 = Arrays.asList(names);
		// nebo zkráceně
		List<String> l2 = Arrays.asList("Larry", "Moe", "Curly");


		// Komplikovanější převody - pole polí
		List<Integer[]> l3 = Arrays.asList(new Integer[][] {
					{ 1 }, { 2 }, { 3 }, { 4 }
				});
	}

}
