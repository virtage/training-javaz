package com.virtage.edu.javaz.ch12collections.sorting;

import com.virtage.edu.javaz.a01register.Person;

import java.util.Comparator;

public class PersonNameComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }
}


