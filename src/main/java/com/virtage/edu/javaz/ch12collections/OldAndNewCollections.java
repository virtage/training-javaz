package com.virtage.edu.javaz.ch12collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class OldAndNewCollections {

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		List<String> numbs = Arrays.asList("one", "two", "three");

		// Convert new -> old collection
		Enumeration<String> enumeration = Collections.enumeration(numbs);

		// New collections		Common impl.		Old collections	counterpart
		// (interfaces)
		// ------------------------------------------------------------------------------------------------
		// Collection<E>
		// Iterator<E>								Enumeration<E> (interface)
		// List<E> 				ArrayList<E>		Vector<E>
		// Set<E> 				HashSet<E>			(no direct counterpart)
		// Map<K,V>				HashMap<K,V>		Dictionary<K,V> (abstract class), Hashtable<K,V> (concrete class)

	}

}
