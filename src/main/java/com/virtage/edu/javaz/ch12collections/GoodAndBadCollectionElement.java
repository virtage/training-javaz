package com.virtage.edu.javaz.ch12collections;

import java.util.ArrayList;
import java.util.List;

public class GoodAndBadCollectionElement {

	static class BadPerson {
		public String name;
		public int age;

		public BadPerson(String name, int age) {
			this.name = name;
			this.age = age;
		}
	}

	static class GoodPerson {
		public String name;
		public int age;

		public GoodPerson(String name, int age) {
			this.name = name;
			this.age = age;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + age;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			GoodPerson other = (GoodPerson) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (age != other.age)
				return false;
			return true;
		}
	}

	public static void main(String[] args) {

		List<BadPerson> badPersons = new ArrayList<>();

		BadPerson hugo =     new BadPerson("Hugo", 20);
		BadPerson kvido =    new BadPerson("Kvido", 30);
		BadPerson hugoClon = new BadPerson("Hugo", 20);

		badPersons.add(hugo);
		badPersons.add(kvido);

		System.out.println(badPersons.contains(hugo)); // true
		System.out.println(badPersons.contains(hugoClon)); // false

		System.out.println("----------------------");

		List<GoodPerson> goodPersons = new ArrayList<>();

		GoodPerson julie =     new GoodPerson("Julie", 15);
		GoodPerson lilie = 	   new GoodPerson("Lilie", 12);
		GoodPerson julieKlon = new GoodPerson("Julie", 15);

		goodPersons.add(julie);
		goodPersons.add(lilie);

		System.out.println(goodPersons.contains(julie)); // true
		System.out.println(goodPersons.contains(julieKlon)); // true

	}

}
