package com.virtage.edu.javaz.ch12collections.sorting;


import com.virtage.edu.javaz.a01register.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingPersons {

    public static void main(String[] args) {

        Person john = new Person("Zac", 30);
        Person jane = new Person("Jane", 29);

        List<Person> friends = new ArrayList<>();
        friends.add(john);
        friends.add(jane);

        //Collections.sort(friends, new PersonNameComparator());

        Comparator<Person> personComparatorReversed = Collections.reverseOrder(new PersonNameComparator());

        Collections.sort(friends, personComparatorReversed);


        System.out.println(friends);
    }
}
