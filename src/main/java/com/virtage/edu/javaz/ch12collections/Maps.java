package com.virtage.edu.javaz.ch12collections;

import java.util.HashMap;
import java.util.Map;

public class Maps {

	public static void main(String[] args) {
		// ** Map **
		Map<String, Integer> mapa = new HashMap<>();
		
		// adding
		mapa.put("Joel", 30);
		mapa.put("Carl", 19);
		mapa.put("Lisa", 25);
		
		// removing
		mapa.remove("Lisa");
		
		
		// looping thought keys
		for (String key : mapa.keySet()) {
			System.out.println(key);
		}
		
		// looping key values (Map.Entry)
		for (Map.Entry<String, Integer> entry : mapa.entrySet()) {
			System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
		}
		
		// same as above but simpler
		for (String klic : mapa.keySet()) {
			System.out.println("key = " + klic + ", value = " + mapa.get(klic));
		}
		
		
	}

}
