package com.virtage.edu.javaz.ch12collections;

import java.util.ArrayList;
import java.util.List;

public class Lists {
	
	public static void main(String[] args) {
		// ** Seznam (list) **
		List<String> list = new ArrayList<>();
		
		// adding
		list.add("Moneyday");
		list.add("Tuesday");
		list.add("Wednesday");
		list.add("Thursday");
		list.add(0, "Monday");
		
		// querying
		list.isEmpty();
		list.indexOf("Tuesday");
		
		// removing
		list.remove(list.indexOf("Tuesday"));
		list.remove("Monday");
		
		// looping
		for (String day : list) {
			System.out.println(day);
		}
	}
}
