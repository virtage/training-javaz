package com.virtage.edu.javaz.ch12collections;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Sets {

	public static void main(String[] args) {
		
		// ** Množiny (set) **
		Set<Integer> set = new HashSet<>();
		set.add(10);
		set.add(10);
		set.add(20);
		
		// querying
		set.contains(20);
		set.isEmpty();
		
		// deleting
		set.remove(20);
		
		// looping
		for (Integer i : set) {
			System.out.println(i);
		}
		
	}

}
